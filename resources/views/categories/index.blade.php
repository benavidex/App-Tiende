@extends('layouts.app')

@section('content')
<div class="panel panel-default">
  <div class="panel-heading pull-right"><a href="{{ route('categories.create') }}" class="btn btn-primary">Nueva categoría</a></div>
  <div class="panel-heading">Categorías</div>
  <div class="panel-body">
    <table id="example" class="table">
      <thead>
        <tr>
          <th>Descripción</th>
          <th>Acciones</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($categories as $cat)
        <tr>
          <td><a href='/categories/{{$cat->id}}'>{{$cat->description}}</a></td>
          <td>
            <a href='/categories/{{$cat->id}}/edit'><button class="btn btn-primary" data-toggle="tooltip" title="Edit"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button></a>
            <form action="/categories/{{$cat->id}}" method="POST"  style="display:inline">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="_method" value="DELETE">

              <button type="submit" class="btn btn-danger" title="Delete">
                <i class="glyphicon glyphicon-trash"></i>
              </button>
            </form>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection
