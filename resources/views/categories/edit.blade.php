@extends('layouts.app')

@section('content')
<div class="panel panel-default col-md-7 col-md-offset-2 pa">
	<div class="panel-heading">Editar categoría</div>
	<div class="panel-body">
		@if (count($errors) > 0)
		<div class="alert alert-danger">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<ul>
				<li>Esa categoría ya está en uso, intentelo de nuevo</li>
			</ul>
		</div>
		@endif
    {!! Form::model($category, [
        'method' => 'PATCH',
        'route' => ['categories.update', $category->id]
    ]) !!}

    <div class="form-group">
        {!! Form::label('description', 'Descripción:', ['class' => 'control-label']) !!}
        {!! Form::text('description', null, ['class' => 'form-control']) !!}
    </div>

    {!! Form::submit('Actualizar categoría', ['class' => 'btn btn-primary']) !!}

    {!! Form::close() !!}
@endsection
