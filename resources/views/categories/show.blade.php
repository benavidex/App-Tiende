@extends('layouts.app')

@section('content')

<h1>{{ $category->description}}</h1>
<hr>
<div class="col-md-6 text-right pull-right">
        {!! Form::open([
            'method' => 'DELETE',
            'route' => ['categories.destroy', $category->id]
        ]) !!}
            {!! Form::submit('¿Eliminar categoría?', ['class' => 'btn btn-danger']) !!}
        {!! Form::close() !!}
    </div>
</div>
<a href="{{ route('categories.index') }}" class="btn btn-info">Ver todas las categorías</a>
<a href="{{ route('categories.edit', $category->id) }}" class="btn btn-primary">Editar categoría</a>

@stop
