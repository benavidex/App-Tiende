@extends('layouts.app')

@section('content')

<div class="panel panel-default col-md-8 col-md-offset-2 pa">
	<div class="panel-heading">Nueva categoría</div>
	<div class="panel-body">
    @if($errors->any())
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
        </div>
    @endif

    @if(Session::has('flash_message'))
        <div class="alert alert-success">
            {{ Session::get('flash_message') }}
        </div>
    @endif

    @if($errors->any())
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
        </div>
    @endif

    {!! Form::open([
        'route' => 'categories.store'
    ]) !!}

    <div class="form-group">
        {!! Form::label('name', 'Nombre:', ['class' => 'control-label']) !!}
        {!! Form::text('description', null, ['class' => 'form-control']) !!}
    </div>

    {!! Form::submit('Crear categoría', ['class' => 'btn btn-primary']) !!}

{!! Form::close() !!}
@endsection
