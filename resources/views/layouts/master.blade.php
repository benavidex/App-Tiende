<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>AppTiende-Admin</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  
  @yield('style')
</head>
    <body>
        <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
  
  <a class="navbar-brand" href="#">AppTiende</a>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav nav">
      <li class="nav-item">
        <a class="nav-link" href="/products">Productos </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/categories">Categorías<span class="sr-only">(current)</span></a>
      </li>
    </ul>
    
  </div>
</nav>
<div class="container">
            @yield('content')
        </div>
        @section('sidebar')
        @show

    </body>
</html>

