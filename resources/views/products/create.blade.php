@extends('layouts.app')

@section('content')

<div class="panel panel-default col-md-8 col-md-offset-2 pa">
	<div class="panel-heading">Nuevo producto</div>
	<div class="panel-body">
    @if($errors->any())
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
        </div>
    @endif

    @if(Session::has('flash_message'))
        <div class="alert alert-success">
            {{ Session::get('flash_message') }}
        </div>
    @endif

    {!! Form::open([
        'route' => 'products.store'
    ]) !!}

    <div class="form-group">
        {!! Form::label('name', 'Producto:', ['class' => 'control-label']) !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('brand', 'Marca:', ['class' => 'control-label']) !!}
        {!! Form::text('brand', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('code', 'Código:', ['class' => 'control-label']) !!}
        {!! Form::text('code', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('available', 'Cantidad disponible:', ['class' => 'control-label']) !!}
        {!! Form::text('available', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('unity', 'Unidad de medida:', ['class' => 'control-label']) !!}
        {!! Form::text('unity', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('portion', 'Cantidad:', ['class' => 'control-label']) !!}
        {!! Form::text('portion', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('price', 'Precio:', ['class' => 'control-label']) !!}
        {!! Form::text('price', null, ['class' => 'form-control']) !!}
    </div>
    <br>
    {!! Form::label('tax', 'Gravado:', ['class' => 'control-label']) !!}
    {{ Form::radio('tax', 1) }}
    {!! Form::label('tax', 'Exento:', ['class' => 'control-label']) !!}
    {{ Form::radio('tax', 0) }}
    <br>
    <label for="category_id">Categoria de producto:</label>

          <select name="category_id"  class="form-control">
            @foreach ($categories as $cat)
            <option value="{{$cat->id}}" >{{$cat->description}}</option>
            @endforeach
          </select>
          <br>

      <div class="form-group">
        {!! Form::label('notes', 'Información adicional:', ['class' => 'control-label']) !!}
        {!! Form::text('notes', null, ['class' => 'form-control']) !!}
    </div>

    {!! Form::submit('Create New Task', ['class' => 'btn btn-primary']) !!}

    {!! Form::close() !!}


  </div>
</div>
</div>
</div>

@endsection
