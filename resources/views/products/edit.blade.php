@extends('layouts.app')

@section('content')
<div class="panel panel-default col-md-7 col-md-offset-2 pa">
	<div class="panel-heading">Editar Producto</div>
	<div class="panel-body">
		@if (count($errors) > 0)
		<div class="alert alert-danger">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<ul>
				<li>Ese código está en uso, intentelo de nuevo</li>
			</ul>
		</div>
		@endif
    {!! Form::model($product, [
        'method' => 'PATCH',
        'route' => ['products.update', $product->id]
    ]) !!}

    <div class="form-group">
        {!! Form::label('name', 'Producto:', ['class' => 'control-label']) !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('brand', 'Marca:', ['class' => 'control-label']) !!}
        {!! Form::text('brand', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('code', 'Código:', ['class' => 'control-label']) !!}
        {!! Form::text('code', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('available', 'Cantidad disponible:', ['class' => 'control-label']) !!}
        {!! Form::text('available', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('unity', 'Unidad de medida:', ['class' => 'control-label']) !!}
        {!! Form::text('unity', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('portion', 'Cantidad:', ['class' => 'control-label']) !!}
        {!! Form::text('portion', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('price', 'Precio:', ['class' => 'control-label']) !!}
        {!! Form::text('price', null, ['class' => 'form-control']) !!}
    </div>
    <br>
    @if($product->tax)
    {!! Form::label('tax', 'Gravado:', ['class' => 'control-label']) !!}
    {{ Form::radio('tax', 1, true) }}
    {!! Form::label('tax', 'Exento:', ['class' => 'control-label']) !!}
    {{ Form::radio('tax', 0, false) }}
    @else
    {!! Form::label('tax', 'Gravado:', ['class' => 'control-label']) !!}
    {{ Form::radio('tax', 1, false) }}
    {!! Form::label('tax', 'Exento:', ['class' => 'control-label']) !!}
    {{ Form::radio('tax', 0, true) }}
    @endif
    <br>
    <label for="category_id">Categoria de producto:</label>
    	<select name="category_id"  class="form-control">
    		@foreach ($categories as $cat)
    		    @if($cat->id == $product->category_id)
    			<option value="{{$cat->id}}" selected="selected">{{$cat->description}}</option>
    			@else
    			<option value="{{$cat->id}}" >{{$cat->description}}</option>
    			@endif
    			@endforeach
    	</select>
    	<br>
    {!! Form::submit('Actualizar producto', ['class' => 'btn btn-primary']) !!}

    {!! Form::close() !!}

    @stop


@include('partials.alerts.errors')

@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif
