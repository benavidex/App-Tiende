@extends('layouts.app')

@section('content')
<div class="panel panel-default">
  <div class="panel-heading pull-right"><a href="{{ route('products.create') }}" class="btn btn-primary">Nuevo Producto</a></div>
  <div class="panel-heading">Productos</div>
  <div class="panel-body">
    <table id="example" class="table">
      <thead>
        <tr>
          <th>Descripción</th>
          <th>Acciones</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($products as $product)
        <tr>
          <td><a href='/products/{{$product->id}}'>{{$product->name}}</a></td>
          <td>
            <a href='/products/{{$product->id}}/edit'><button class="btn btn-primary" data-toggle="tooltip" title="Edit"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button></a>
            <form action="/products/{{$product->id}}" method="POST"  style="display:inline">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="_method" value="DELETE">

              <button type="submit" class="btn btn-danger" title="Delete">
                <i class="glyphicon glyphicon-trash"></i>
              </button>
            </form>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
