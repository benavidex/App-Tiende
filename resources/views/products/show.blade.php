@extends('layouts.app')

@section('content')

<h1>{{ $product->name}}-{{ $product->brand}}</h1>
<p class="lead text-center">Código: {{ $product->code }}</p>
<p class="lead text-center">{{ $product->portion }}{{ $product->unity }}</p>
<p class="lead text-center">{{ $product->price }} colones</p>
<hr>
<div class="col-md-6 text-right pull-right">
        {!! Form::open([
            'method' => 'DELETE',
            'route' => ['products.destroy', $product->id]
        ]) !!}
            {!! Form::submit('¿Borrar producto?', ['class' => 'btn btn-danger']) !!}
        {!! Form::close() !!}
    </div>
</div>
<a href="{{ route('products.index') }}" class="btn btn-info">Ver todos los productos</a>
<a href="{{ route('products.edit', $product->id) }}" class="btn btn-primary">Editar producto</a>

@stop
