<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
|
|
*/
//Route::get('user', 'UserController@getAuthUser');
Route::get('test', 'UserController@getTest');
Route::post('auth/register', 'UserController@register');
Route::post('auth/login', 'UserController@login');
Route::group(['middleware' => 'jwt.auth'], function () {
    Route::get('user', 'UserController@getAuthUser');
    Route::post('user', 'UserController@update');
    Route::post('password', 'UserController@updatePassword');
    Route::post('orders','OrderController@store');

});
Route::get('productInfo/{id}','ProductController@productInfo');
Route::get('allProducts','ProductController@products');
Route::get('orders/{id}','OrderController@show');
Route::get('user_orders/{user_id}','OrderController@index');
Route::get('user_confirmed_orders/{user_id}','OrderController@getConfirmedOrders');
