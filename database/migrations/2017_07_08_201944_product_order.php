<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductOrder extends Migration
{
  public function up()
  {
    Schema::table('product_order', function($table) {
      $table->create();
      $table->increments('id');
      $table->integer('order_id')->unsigned();
      $table->integer('product_id')->unsigned();
      $table->float('subtotal');
      $table->integer('quantity');
      $table->timestamps();

      $table->foreign('order_id')->references('id')->on('orders');
      $table->foreign('product_id')->references('id')->on('products');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::drop('product_order');
  }
}
