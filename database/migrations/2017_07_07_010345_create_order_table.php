<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    //Order table
    Schema::table('orders', function($table) {
      $table->create();
      $table->increments('id');
      $table->string('description')->nullable();
      $table->date('date');
      $table->integer('user_id')->unsigned();
      $table->float('total')->default(999);
      $table->boolean('confirmed')->default(0);
      $table->timestamps();

      $table->foreign('user_id')->references('id')->on('users');
    });
  }
  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::drop('orders');
  }
}
