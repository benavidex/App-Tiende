<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {

    DB::table('products')->insert([
      'name' => 'arroz',
      'brand'=> 'Tio Pelón',
      'unity'=> 'g',
      'notes'=>'saludable',
      'portion'=>1000,
      'price'=> 225,
      'available'=> 5,
      'code'=> 7441074,
      'tax'=>false,
      'category_id'=>3
    ]);

    DB::table('products')->insert([
      'name' => 'frijol negro',
      'brand'=> 'Tio Pelón',
      'unity'=> 'g',
      'notes'=>'escogido',
      'portion'=>1000,
      'price'=> 905,
      'available'=> 3,
      'code'=> 7441076,
      'tax'=>false,
      'category_id'=>3
    ]);

    DB::table('products')->insert([
      'name' => 'Atún Lomo en trocitos',
      'brand'=> 'Sardimar',
      'unity'=> 'g',
      'notes'=>'en aceite',
      'portion'=>140,
      'price'=> 934,
      'available'=> 20,
      'code'=> 7441075,
      'tax'=>true,
      'category_id'=>2
    ]);
    DB::table('products')->insert([
      'name' => 'Leche descremada',
      'brand'=> 'Dos Pinos',
      'unity'=> 'ml',
      'notes'=>'2% de grasa',
      'portion'=>1000,
      'price'=> 870,
      'available'=> 10,
      'code'=> 7441073,
      'tax'=>false,
      'category_id'=>3
    ]);

    DB::table('products')->insert([
      'name' => 'Aleve',
      'brand'=> 'Bayer',
      'unity'=> 'Tableta',
      'notes'=>'Para el dolor de espalda',
      'portion'=>12,
      'price'=> 3220,
      'available'=> 8,
      'code'=> 7441080,
      'tax'=>true,
      'category_id'=>4
    ]);

    DB::table('products')->insert([
      'name' => 'Dorival',
      'brand'=> 'Bayer',
      'unity'=> 'grageos',
      'notes'=>'Alivia el dolor y la irritación menstrual',
      'portion'=>12,
      'price'=> 4280,
      'available'=> 15,
      'code'=> 7441081,
      'tax'=>true,
      'category_id'=>4
    ]);
    DB::table('products')->insert([
      'name' => 'Atún lomo en trocitos x3',
      'brand'=> 'Sardimar',
      'unity'=> 'g',
      'notes'=>'en aceite',
      'portion'=>420,
      'price'=> 1868,
      'available'=> 11,
      'code'=> 7441099,
      'tax'=>true,
      'category_id'=>1
    ]);
  }
}
