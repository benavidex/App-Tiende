<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('categories')->insert([
            'description' => 'promociones',
        ]);
        DB::table('categories')->insert([
            'description' => 'enlatados',
        ]);
        DB::table('categories')->insert([
            'description' => 'abarrotes',
        ]);
        DB::table('categories')->insert([
            'description' => 'cuidado personal',
        ]);
    }
}
