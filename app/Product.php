<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
  use SoftDeletes;
  protected $dates = ['deleted_at'];
  protected $fillable = [
    'name','brand','code','unity', 'price', 'available','tax','category_id',
    'notes','portion'
  ];
  public function product_order(){
    return $this->hasMany('Product_Order');
  }
  public function category(){
    return $this->belongsTo('Category');
  }

}
