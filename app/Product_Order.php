<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Eloquent;

class Product_Order extends Eloquent
{
    //
    protected $table = "product_order";
    protected $fillable = [
      'order_id','product_id','quantity','subtotal',
    ];

    public function product(){
      return $this->belongsTo('Product');
    }
    public function order(){
      return $this->belongsTo('Order');
    }


}
