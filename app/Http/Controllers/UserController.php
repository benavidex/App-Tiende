<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use App\User;
use JWTAuthException;

class UserController extends Controller
{
  private $user;
  public function __construct(User $user){
    $this->user = $user;
  }

  public function register(Request $request){
    $user = $this->user->create([
      'name' => $request->get('name'),
      'lastname' => $request->get('lastname'),
      'address' => $request->get('address'),
      'phone' => $request->get('phone'),
      'email' => $request->get('email'),
      'password' => bcrypt($request->get('password'))
    ]);
    return response()->json(['status'=>true,'message'=>'User created successfully','data'=>$user]);
  }

  public function login(Request $request){
    $credentials = $request->only('email', 'password');
    $token = null;
    try {
      if (!$token = JWTAuth::attempt($credentials)) {
        return response()->json(['invalid_email_or_password'], 422);
      }
    } catch (JWTAuthException $e) {
      return response()->json(['failed_to_create_token'], 500);
    }
    //return Response::json('token');
    return response()->json(compact('token'));
  }

  public function getAuthUser(Request $request){
    //return response()->json($request->header('token'));
    $user = JWTAuth::toUser($request->token);
    //return Response::json($user);
    return response()->json($user);
  }

  public function update(Request $request)
  {
    try {
      //dd($request->all());
      $user = JWTAuth::toUser($request->token);
      $user->update($request->all());
      return response()->json(['user_updated'], 200);
    } catch (Exception $e) {
      return response()->json(['failed_to_update_user'], 500);

    }
  }
  public function updatePassword( Request $request)
  {
     $user = JWTAuth::toUser($request->token);
      $user->fill([
        'password' => bcrypt($request->get('password'))
          //'password' => Hash::make($request->password)
      ])->save();

      return response()->json(['password_updated'], 200 );


  }
}
