<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Eloquent;
use App\Order;
use App\Product_Order;
use App\Product;

class OrderController extends Controller
{
  public function __construct()
  {
      //$this->middleware('auth');
  }
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index($user_id)
  {
    //$orders= Order->user();
    $orders = Order::where('user_id',$user_id)->select("id","description","date",
    "total")->where('confirmed', '=', false)->orderBy('date','desc')->take(12)->get();
      return response()->json(json_decode($orders));
  }
  public function getConfirmedOrders($user_id)
  {
    $orders = Order::where('user_id',$user_id)->select("id","description","date",
    "total")->where('confirmed', '=', true)->orderBy('date','desc')->take(12)->get();

      //return response()->json(['status'=>true,'message'=>'Orders','data'=>json_decode($orders)]);
      return response()->json(json_decode($orders));
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    //
    $correct=false;
    try {
      if ($request->get('confirmed')=="True") {
        $confirmed=true;
      }else {
        $confirmed=false;
      }
      $order = Order::create([
        'user_id' => $request->get('user_id'),
        'date' =>date('Y-m-d H:i:s'),
        'description' => $request->get('description'),
        'confirmed' => $confirmed,
        'total' => 0,//<---------------------------trigger implemented
      ]);
      //$products=$request->get('idProducts');
      //dd($products);
      $idProducts=explode(",",$request->get('idProducts'));
      $qtyProducts=explode(",",$request->get('qtyProducts'));
      for ($i=0; $i <count($idProducts); $i++) {
        $current_price = Product::where('id', $idProducts[$i])->select('price', 'tax','available')->get();
        $cost = 0;
        if ($current_price[0]['available']>=$qtyProducts[$i]) {
          if ($current_price[0]['tax']) {
            $cost=($qtyProducts[$i]*1.13*$current_price[0]['price']);
          }else{
            $cost=($qtyProducts[$i]*$current_price[0]['price']);
          }

          $product_order=Product_Order::create([
            'order_id'=>$order->id,
            'product_id'=>$idProducts[$i],
            'quantity'=>$qtyProducts[$i],
            'subtotal'=>$cost,
          ]);
          if ($confirmed) {
            $newQty=$current_price[0]['available']-$qtyProducts[$i];
            $current_product=Product::where('id', $idProducts[$i])->update(['available' => $newQty]);
          }

          //dd($current_product->available);
          //$current_product->available=$current_product->available-$qtyProducts[$i];
          //$current_product->save();
          $correct=true;
        }else{
          $correct=false;
        }
      }
      if ($correct){
        return response()->json(['status'=>true,'message'=>'Order created','data'=>$order]);
      }else {
        return response()->json(['status'=>false,'message'=>'Order NOT created','data'=>$order]);
      }
      //return response()->json($request->all());

    } catch (Exception $e) {
      return response()->json($e);
    }
  }

  /**
  * Display the specified resour
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
    $product_orders = Product_Order::where('order_id', $id)->select('product_id','quantity')->get();

    return response()->json(json_decode($product_orders));
    //return response()->json(['status'=>true,'message'=>'Order info','data'=>[$order,$arrayProduct,$arrayQty,$arraySubtotal]]);
    //dd(count($arrayProduct));//
    //dd($arrayProduct[0][0]['name']);//

  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    //
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    //

  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    //
  }
}
