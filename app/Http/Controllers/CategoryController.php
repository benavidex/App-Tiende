<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Session;

class CategoryController extends Controller
{
    //
    public function index()
    {
      //
      $categories=Category::all();
      return view('categories.index', compact('categories'));

    }

    public function create(){
      return view('categories.create');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {

      $this->validate($request, [
          'description' => 'required'
        ]);

      $input = $request->all();

      category::create($input);

      Session::flash('flash_message', 'Categoría agregada correctamente!');

      return redirect()->back();
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
      //
      $category = Category::findOrFail($id);
      return view('categories.show',compact('category'));

    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
      //
      $category = Category::findOrFail($id);
      return view('categories.edit',compact('category'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
      //
      $categori = Category::findOrFail($id);

      $this->validate($request, [
          'description' => 'required'
      ]);

      $input = $request->all();

      $categori->fill($input)->save();

      Session::flash('flash_message', 'Categoría actualizado!');
      //dd($request->all());

      return redirect()->back();
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
      //
      $category = Category::findOrFail($id);

      $category->delete();

      Session::flash('flash_message', 'Categoría eliminado!');

      return redirect()->route('categories.index');
    }
}
