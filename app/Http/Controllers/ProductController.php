<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Product;
use App\Category;

class ProductController extends Controller
{
  public function __construct()
  {
    //$this->middleware('auth', ['except' => ['productInfo','allProducts']]);
      //$this->middleware('auth')->except('productInfo');
      //$this->middleware('auth')->except('allProducts');
  }
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    //
    $products=Product::all();
    return view('products.index', compact('products'));

  }

  public function create(){
    $categories=Category::all();
    return view('products.create',compact('categories'));
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {

    $this->validate($request, [
        'name' => 'required',
        'brand' => 'required',
        'code'=>'required',
        'unity'=>'required',
        'price'=>'required',
        'available'=>'required',
        'tax'=>'required',
        'category_id'=>'required',
        'portion'=>'required'
    ]);

    $input = $request->all();

    Product::create($input);

    Session::flash('flash_message', 'Producto agregado correctamente!');

    return redirect()->back();
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
    $product = Product::findOrFail($id);
    return view('products.show')->withProduct($product);

  }

  public function productInfo($id)
  {
    //
    $product  = Product::find($id);
    return response()->json($product);

  }
  public function products()
  {
    //
    $products  = Product::select('id','name','brand', 'category_id')->get();
    return response()->json($products);

  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    //
    $categories=Category::all();
    $product = Product::findOrFail($id);
    return view('products.edit',compact('product', 'categories'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    //
    $product = Product::findOrFail($id);

    $this->validate($request, [
        'name' => 'required',
        'brand' => 'required',
        'code'=>'required',
        'unity'=>'required',
        'price'=>'required',
        'available'=>'required',
        'tax'=>'required',
        'category_id'=>'required',
        'portion'=>'required'
    ]);

    $input = $request->all();

    $product->fill($input)->save();

    Session::flash('flash_message', 'Producto actualizado!');
    //dd($request->all());

    return redirect()->back();
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    //
    $product = Product::findOrFail($id);

    $product->delete();

    Session::flash('flash_message', 'Producto eliminado!');

    return redirect()->route('products.index');
  }
}
