<?php

namespace App;
use Eloquent;
use Illuminate\Database\Eloquent\Model;

class Order extends Eloquent
{
  public function user(){
    return $this->belongsTo('User');
  }
  public function product_order(){
    return $this->hasMany('Product_Order');
  }

  protected $fillable = [
    'date','description','confirmed','user_id','total',
  ];



}
